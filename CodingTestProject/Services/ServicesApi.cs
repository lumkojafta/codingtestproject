﻿using Newtonsoft.Json;
using NUnit.Framework;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using static CodingTestProject.Model.ServicesItem;

namespace CodingTestProject.Services
{
    [TestFixture]
    public class ServicesApi
    {
        public static void StatusCodeTest()
        {
            string jsonFilePath = @"C:\Users\lumkilej\OneDrive - BCX\Desktop\endpoints.json";
            // string jsonFilePath = ConfigurationManager.AppSettings["path"];

            using (StreamReader r = new StreamReader(jsonFilePath))
            {
                string json = r.ReadToEnd();
                JavaScriptSerializer jss = new JavaScriptSerializer();
                var Object = JsonConvert.DeserializeObject<ApiServices>(json);

                if (Object != null)
                {


                    foreach (var JsonObject in Object.services)
                    {
                        
                        foreach (var apicall in JsonObject.endpoints)
                        {
                            var client = new RestClient(JsonObject.baseURL + apicall.resource);

                            var query = new RestRequest(JsonObject.baseURL + apicall.resource, Method.GET);

                            IRestResponse softresponse = client.Execute(query);

                            // assert
                            Assert.That(softresponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));

                            Console.WriteLine(softresponse.Content);

                            Console.ReadKey();

                        }

                    }
                }
            }
        }
        public static void ContentTypeTest()
        {
            try
            {
                string jsonFilePath = @"C:\Users\lumkilej\OneDrive - BCX\Desktop\endpoints.json";
                // string jsonFilePath = ConfigurationManager.AppSettings["path"];

                using (StreamReader r = new StreamReader(jsonFilePath))
                {
                    string json = r.ReadToEnd();
                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    var Object = JsonConvert.DeserializeObject<ApiServices>(json);

                    if (Object != null)
                    {


                        foreach (var JsonObject in Object.services)
                        {

                            foreach (var apicall in JsonObject.endpoints)
                            {
                                var client = new RestClient(JsonObject.baseURL + apicall.resource);

                                var query = new RestRequest(JsonObject.baseURL + apicall.resource, Method.GET);

                                IRestResponse softresponse = client.Execute(query);

                                // assert
                                Assert.That(softresponse.StatusCode, Is.EqualTo("application/json"));

                                Console.WriteLine(softresponse.Content);

                                Console.ReadKey();

                            }

                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            
            }
        }    
          
    }
}

